<?php

/**
 * @file
 * Hook provided by the Views Access module.
 */

/**
 * Inform the Views module of the user's permissions for viewing this view.
 *
 * Functions implementing this hook shoudl return either TRUE or FALSE, or
 * nothing at all. When multiple functions respond to this hook, if any of them
 * return TRUE, access to the view will be granted.
 *
 * @param $view_name
 *   The name of the view being rendered
 * @param $display_name
 *   The display name of the view being rendered.
 *
 * @return
 *   TRUE or FALSE, though FALSE is implied.
 */
function hook_views_access($view, $account = NULL) {
  if ($view->name == 'my-view-name') {
    return TRUE;
  }
}
