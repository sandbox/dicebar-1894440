<?php

/**
 * @file
 * Views access plugin that allows hook_views_access() to determine access to a
 * view.
 */
class views_plugin_access_views_access extends views_plugin_access {
  function access($account) {
    return views_access_callback(array(
      $this->view,
    ), $account);
  }

  function get_access_callback() {
    return array(
      'views_access_callback',
      array(
        $this->view,
      ),
    );
  }
}
